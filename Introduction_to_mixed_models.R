# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# November 2021

# Introduction to mixed models

## Linear regression

# Define Y
Y<-c(40,50,50,70,90,95)

# Define X1
X1<-c(8,12,11,12,14,15)

# Define X2
X2<-c(32,40,38,60,70,70)
  
# Define model using lm
modele<-lm(Y~X1+X2)
modele

# Result
# Call:
# lm(formula = Y ~ X1 + X2)

# Coefficients:
#   (Intercept)           X1           X2  
# -11.864        1.704        1.108


# We have the variable Y that we want to explain with factors X1 and X2 here

#  Summary of the modele variable
summary(modele)

# Result
# Call:
# lm(formula = Y ~ X1 + X2)

# Residuals:
#  1       2       3       4       5       6 
# 2.7743 -2.9065  1.0137 -5.0668  0.4447  3.7405 

# Coefficients:
#             Estimate Std. Error t value Pr(>|t|)  
# (Intercept) -11.8642    11.0795  -1.071   0.3627  
# X1            1.7042     1.6880   1.010   0.3870  
# X2            1.1080     0.2427   4.566   0.0197 *
#  ---
#  Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

# Residual standard error: 4.36 on 3 degrees of freedom
# Multiple R-squared:  0.9782,	Adjusted R-squared:  0.9637 
# F-statistic: 67.43 on 2 and 3 DF,  p-value: 0.00321


# In the call, the Y is our dependant variable and the X1 and X2 are our independent  variables (predictor)

# The residuals are the difference between the actual values and the predicted values

# To understand what the coefficients are, we need to go back to what we are 
# actually trying to do when we build a linear model. We are looking to build 
# a generalized model in the form of y=mx+b, where b is the intercept and m is 
# the slope of the line
# Because we often don't have enough information or data to know the exact 
# equation that exists in the wild, we have to build this equation by 
# generating estimates for both the slope and the intercept. These estimates 
# are most often generated through the ordinary least squares method, 
# which means that the regression model finds the line that fits the points 
# in such a way that it minimizes the distance between each point and the line
# (minimizes the sum of the squared differences between the actual values 
# and the predicted values).

# With The coefficient Estimate we obtain our coefficients. 
# Where the line meets the y-axis is our intercept (b) and the slope of 
# the line is our m. So the equation for our mode : We'll substitute X1 and X2
# for m and (Intercept) for b:
# y = 1.7042(x) + -11.8642  for X1
# y = 1.1080 (x) + -11.8642 for X2


# The standard error of the coefficient is an estimate of the standard 
# deviation of the coefficient. In effect, it is telling us how much 
# uncertainty there is with our coefficient. The standard error is often 
# used to create confidence intervals. 

# The t-statistic is the coefficient divided by the standard error. 
# In general, we want our coefficients to have large t-statistics, 
# because it indicates that our standard error is small in comparison 
# to our coefficient. We are saying that the coefficient is X standard 
# errors away from zero

# The p-value is calculated using the t-statistic from the T distribution.
# The p-value, in association with the t-statistic, help us to understand 
# how significant our coefficient is to the model. In practice, 
# any p-value below 0.05 is usually deemed as significant.It means we are 
# confident that the coefficient is not zero, meaning the coefficient does 
# in fact add value to the model by helping to explain the variance within 
# our dependent variable. 


# The residual standard error is a measure of how well the model fits the data


# The Multiple R-squared value is most often used for simple linear regression
# (one predictor). It tells us what percentage of the variation within our 
# dependent variable that the independent variable is explaining. 
# In other words, it's another method to determine how well our model 
# is fitting the data.
# In our example above, X1+X2 explain about 97.82% of the variation within Y,
# our dependent variable


# The null hypothesis is that there is no relationship between the 
# dependent variable and the independent variable(s) and the alternative 
# hypothesis is that there is a relationship. Said another way, the null 
# hypothesis is that the coefficients for all of the variables in your model 
# are zero. The alternative hypothesis is that at least one of them is 
# not zero. The F-statistic and overall p-value help us determine the 
# result of this test.


# The F-statistic and overall p-value help us determine the result of 
# this test. Looking at the F-statistic alone can be a little misleading 
# depending on how many variables are in our test. If we have a lot of 
# independent variables, it's common for an F-statistic to be close to 
# one and to still produce a p-value where we would reject the null 
# hypothesis. However, for smaller models, a larger F-statistic 
# generally indicates that the null hypothesis should be rejected. 



# Here we have an F-statistic that is large an a p-value very small. 
# So we can reject the null hypothesis and conclude that there is a relationship between
# Y and X1+X2


## Residuals

# Load needed library
library(MASS)

# Use of the studres() function on the variable modele
# Extract Studentized Residuals from a Linear Model
sresid<-studres(modele)
sresid
# Result
#         1          2          3          4          5          6 
#     1.7158651 -1.0601898  0.2438142 -2.3143135  0.1091141  1.3052281 


# Use of par
par(mfrow=c(1,1))
# Plot residuals with sresid on the y axis and modele$fitted.values on the x axis
plot(modele$fitted.values,sresid)
abline(0,0)

# Shapiro-Wilk test
shapiro.test(sresid)
# Result
# data:  sresid
# W = 0.95314, p-value = 0.7656

# p value not indicative so the sample follows a  normal law


## Colinearity

# Load car library
library(car)

# Use of the vif() function on the modele variable
vif(modele)
# Result
#       X1       X2 
# 4.496076 4.496076 

# A value of 1 means that the predictor is not correlated with other variables. 
# The higher the value, the greater the correlation of the variable with other 
# variables


# Anova with aov() function on iris data
modele<-aov(Sepal.Width~Species,data=iris)
modele
# Result
# Call:
# aov(formula = Sepal.Width ~ Species, data = iris)

# Terms:
#   Species Residuals
# Sum of Squares  11.34493  16.96200
# Deg. of Freedom        2       147

# Residual standard error: 0.3396877
# Estimated effects may be unbalanced


# Summary of modele variable
summary(modele)
# Result
#              Df Sum Sq Mean Sq F value Pr(>F)    
# Species       2  11.35   5.672   49.16 <2e-16 ***
#   Residuals   147  16.96   0.115                   
# ---
#   Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

# As the p-value is less than the significance level 0.05, 
# we can conclude that there are significant differences between the groups 
# highlighted with "*" in the model summary.

# In one-way ANOVA test, a significant p-value indicates that some of 
# the group means are different, but we don't know which pairs of groups 
# are different.


# As the ANOVA test is significant, we can compute Tukey HSD 
# (Tukey Honest Significant Differences) for performing multiple pairwise-comparison
# between the means of groups.
# The function TukeyHD() takes the fitted ANOVA as an argument.

# Use of TukeyHSD function on modele variable  
TukeyHSD(modele)
# Result
#  Tukey multiple comparisons of means
# 95% family-wise confidence level

# Fit: aov(formula = Sepal.Width ~ Species, data = iris)

# $Species
#           diff         lwr        upr     p adj
# versicolor-setosa    -0.658 -0.81885528 -0.4971447 0.0000000
# virginica-setosa     -0.454 -0.61485528 -0.2931447 0.0000000
# virginica-versicolor  0.204  0.04314472  0.3648553 0.0087802

# diff: difference between means of the two groups
# lwr, upr: the lower and the upper end point of the confidence interval at 95% (default)
# p adj: p-value after adjustment for the multiple comparisons.


# Use of leveneTest() function on iris data
leveneTest(Sepal.Width~Species,data=iris)
# Result
# Levene's Test for Homogeneity of Variance (center = median)
# Df F value Pr(>F)
# group   2  0.5902 0.5555
#        147 

# Bartlett's test is used for testing homogeneity of variances in k samples, 
# where k can be more than two. It's adapted for normally distributed data. 
# The Levene test is a more robust alternative to the Bartlett test when 
# the distributions of the data are non-normal.


# Shapiro-Wilk test on the residuals of modele variable
shapiro.test(modele$residuals)
# Result
# 	Shapiro-Wilk normality test

# data:  modele$residuals
# W = 0.98948, p-value = 0.323














