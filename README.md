# Intro_to_mixed_models


In the R file provided there are some operation about mixed models :


- Linear regression (lm)


- Residuals (studres)


- Shapiro-Wilk test (shapiro.test)


- Colinearity with car library/data (vif)


- Anova with iris data (aov)


- Tukey Honest Significant Differences with iris data (TukeyHSD)


- Levene test (test homogeneity of variances in k samples) with iris data (leveneTest)


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2021

